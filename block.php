<?php
class HeadwayFeaturedPostsBlock extends HeadwayBlockAPI {
	
	
	public $id = 'featured-posts';
	
	public $name = 'FeaturedPosts Category';
	
	public $options_class = 'HeadwayFeaturedPostsBlockOptions';
		

	function init() {

	// add_action('wp_ajax_headway_featured_posts_infinite_scroll', array(__CLASS__, 'infinite_scroll_content'));
	// 	add_action('wp_ajax_nopriv_headway_featured_posts_infinite_scroll', array(__CLASS__, 'infinite_scroll_content'));

	}


	function enqueue_action($block_id) {

		/* CSS */
		wp_enqueue_style('headway-featured-posts', plugins_url(basename(dirname(__FILE__))) . '/css/featured-posts.css');

		/* JS */
		wp_enqueue_script('headway-carousfredsel', plugins_url(basename(dirname(__FILE__))) . '/js/min.caroufredsel.js', array('jquery'));
		wp_enqueue_script('headway-flipper-posts', plugins_url(basename(dirname(__FILE__))) . '/js/flipper.js', array('jquery'));
		wp_enqueue_script('headway-easing-posts', plugins_url(basename(dirname(__FILE__))) . '/js/script.easing.js', array('jquery'));

		/* Variables */
		// wp_localize_script('headway-featured-posts', 'HeadwayFeaturedPosts', array(
		// 	'ajaxURL' => admin_url('admin-ajax.php')
		// ));

	}

    function dynamic_js($block_id, $block = false) {

		if ( !$block )
			$block = HeadwayBlocksData::get_block($block_id);

		$js="jQuery(document).ready(function($) {

					$('.flipper').each(function(){

				    	var flipper = $(this)
						,	scrollSpeed
						, 	easing
					//	, 	shown = flipper.data('shown') || 3
						,	scrollSpeed = flipper.data('scroll-speed') || 1200
						,	easing = flipper.data('easing') || 'linear'
					//	,	easing = flipper.data('easing') || 'easeInOutCubic'

				    	flipper.carouFredSel({
				    		circular: 	true
				    		, responsive: true
							, height: 'variable'
							, onCreate: function(){
							}

							, items       : {
								width : 353,
								height: 'variable',
						        visible     : {
						            min         : 1
						            , max         : shown
						        }
						    }

						    , swipe       : {
						        onTouch     : true
						    }
						    , scroll: {
						    	easing          : easing
					            ,duration        : scrollSpeed
						        ,fx              : 'crossfade'
								,pauseOnHover     : true
						    }

							,auto     : {
							      play            : true
							     ,timeoutDuration :  15000 //5 * auto.duration
					        ,duration        :  scroll.duration
							}
			
					        , prev    : {
						        button  : function() {
						           return flipper.parents('.flipper-wrap').prev('.flipper-heading').find('.flipper-prev');
						        }
					    	}
						    , next    : {
					       		button  : function() {
						           return flipper.parents('.flipper-wrap').prev('.flipper-heading').find('.flipper-next');
						        }
						    }
						    , auto    : {
						    	play: true
						    }
			
					    }).addClass('flipper-loaded').animate({'opacity': 1},1300);
	
						//$.plCommon.plVerticalCenter('.flipper-info', '.pl-center', -20)
				    });";

//	return $js;
	}

	function dynamic_css($block_id) {

	//	return '#block-' . $block_id . ' .featured-posts-featured { margin-bottom: ' . parent::get_setting($block_id, 'featured-bottom-margin', 15) . 'px; }';

	}

	/** 
	 * Anything in here will be displayed when the block is being displayed.
	 **/
	function content($block) {
	// echo	$post_type = parent::get_setting($block, 'post-type', 'post');
	// 
		$post_type = parent::get_setting($block, 'post-type', false);
		$html_tag = parent::get_setting($block, 'title-html-tag', 'h3');
		
		$linked = parent::get_setting($block,'title-link', true);
		$title = parent::get_setting($block, 'title');

		$columns = parent::get_setting($block, 'columns', 3);
		$approx_featured_width = (HeadwayBlocksData::get_block_width($block) / $columns);

		/* Element Visibility */
			// $show_images = parent::get_setting($block, 'show-images', true);
			// $show_titles = parent::get_setting($block, 'show-titles', true);

			/* Meta */
				// $show_author = parent::get_setting($block, 'show-author', false);
				// $show_datetime = parent::get_setting($block, 'show-datetime', false);
				// $datetime_verb = parent::get_setting($block, 'datetime-verb', 'Posted');
				// $relative_times = parent::get_setting($block, 'relative-times', true);

			/* Content */
				$show_continue = parent::get_setting($block, 'show-continue', false);
				$content_to_show = parent::get_setting($block, 'content-to-show', 'excerpt');

		/* Images */
			$crop_images_vertically = parent::get_setting($block, 'crop-vertically', false);

		// /* Social Stuff */
		// 	/* FeaturedPoststerest */
		// 		$show_featuredterest_button = parent::get_setting($block, 'show-featured-erest-button', false);
		// 
		// 	/* Twitter */
		// 		$show_twitter_button = parent::get_setting($block, 'show-twitter-button', false);
		// 
		// 		$twitter_username = parent::get_setting($block, 'twitter-username', null);
		// 		$twitter_hashtag = parent::get_setting($block, 'twitter-hashtag', null);
		// 
		// 	/* Facebook */
		// 		$show_facebook_button = parent::get_setting($block, 'show-facebook-button', false);
		// 		$facebook_button_verb = parent::get_setting($block, 'facebook-button-verb', 'like');
		/* End Social Stuff */


		/* Setup Query */
			$query_args = array();

			/* Pagination */
				$paged_var = get_query_var('paged') ? get_query_var('paged') : get_query_var('page');

				if ( (parent::get_setting($block, 'paginate', true) || parent::get_setting($block, 'infinite-scroll', true)) && (headway_get('featured-posts-page') || $paged_var) )
					$query_args['paged'] = headway_get('featured-posts-page') ? headway_get('featured-posts-page') : $paged_var;

			/* Categories */
				if ( parent::get_setting($block, 'categories-mode', 'include') == 'include' ) 
					$query_args['category__in'] = parent::get_setting($block, 'categories', array());

				if ( parent::get_setting($block, 'categories-mode', 'include') == 'exclude' ) 
					$query_args['category__not_in'] = parent::get_setting($block, 'categories', array());	

			$query_args['post_type'] = parent::get_setting($block, 'post-type', false);

			/* FeaturedPosts limit */
				$query_args['posts_per_page'] = parent::get_setting($block, 'total', 3);

			/* Author Filter */
				if ( is_array(parent::get_setting($block, 'author')) )
					$query_args['author'] = trim(implode(',', parent::get_setting($block, 'author')), ', ');

			/* Order */
				$query_args['orderby'] = parent::get_setting($block, 'order-by', 'date');
				$query_args['order'] = parent::get_setting($block, 'order', 'DESC');

			/* Status */
				$query_args['post_status'] = 'publish';

			/* Query! */
				$posts = new WP_Query($query_args);

				global $paged; /* Set paged to the proper number because WordPress pagination SUCKS!  ANGER! */
				$paged = $paged_var;
		/* End Query Setup */

/** 
	
	DEBUG 

**/		
	if (parent::get_setting($block, 'debug') == true) 	{
		print_r($query_args);
	}

// Link na archiv post type
  function get_archive_link( $post_type ) {
    global $wp_post_types;
    $archive_link = false;
    if (isset($wp_post_types[$post_type])) {
      $wp_post_type = $wp_post_types[$post_type];
      if ($wp_post_type->publicly_queryable)
        if ($wp_post_type->has_archive && $wp_post_type->has_archive!==true)
          $slug = $wp_post_type->has_archive;
        else if (isset($wp_post_type->rewrite['slug']))
          $slug = $wp_post_type->rewrite['slug'];
        else
          $slug = $post_type;
      $archive_link = get_option( 'siteurl' ) . "/{$slug}/";
    }
    return apply_filters( 'archive_link', $archive_link, $post_type );
  }
  // END Link na archiv post type
		if(!empty($posts)) { the_post( $post ); ?>
				<div class="flipper-heading">
					<div class="flipper-title">
						<?php

						if($linked){
							printf('<%s><a href="%s">%s</a></%s>', $html_tag, get_archive_link($post_type), $title,  $html_tag );

						} else {
						printf('<%s>%s</%s>', $html_tag, $title,  $html_tag );

							
						}
						?>
					</div>
					<?php 
						$prev_nav = parent::get_setting($block, 'prev-nav', 'fa fa-caret-left');
						$next_nav = parent::get_setting($block, 'next-nav', 'fa fa-caret-right');
					 ?>
					<a class="flipper-prev" href="#"><i class="<?php echo $prev_nav; ?>"></i></a>
			    	<a class="flipper-next" href="#"><i class="<?php echo $next_nav; ?>"></i></a>
				</div>
				<div class="flipper-wrap">
				<ul class="flipper-items text-align-center flipper" data-scroll-speed="1400" data-easing="easeInOutQuart" data-shown="6">
		<?php } ?>

			<?php
			if(!empty($posts)):
				while ( $posts->have_posts() ) : $posts->the_post();
				?>
			<li>
				<div class="flipper-item fix">
					<?php
					if ( has_post_thumbnail() ) {
						echo get_the_post_thumbnail( $post->ID, 'featured-thumb', array('title' => ''));
					} else {
						echo '<img height="400" width="600" src="/missing-thumb.jpg" alt="no image added yet." />';
						}
						 ?>

					<div class="flipper-info-bg"></div>
					<?php
					if(!$spiker_hide_link_to_post ){
						?>
						
						<a class="flipper-info pl-center-inside" href="<?php echo get_permalink();?>">
							<div class="pl-center">
							<?php
								$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured' );
								printf('<div class="info-text">%s</div>', __("Zobraziť", 'headway'));
							?>
							</div>
						</a>

						<?php	} else{?>
						<div class="pl-center">
						<?php
							$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured' );
							//printf('<div class="info-text">%s</div>', __("View", 'pagelines'));
						?>
						</div>
					<?php }
					?>
				</div><!--work-item-->
				<div class="flipper-meta">
					<h4 class="flipper-post-title"><?php the_title(); ?></h4>
					<?php edit_post_link(); ?>
					<?php //the_content("More...");  					the_excerpt();?>
				</div>
			</li>
			<?php 
		endwhile;
		endif;
			if(!empty($posts))
		 		echo '</ul>
			<div class="clear"></div></div>';
}

////

	function my_excerpt($limit) {
	  $excerpt = explode(' ', get_the_excerpt(), $limit);
	  if (count($excerpt)>=$limit) {
	    array_pop($excerpt);
	    $excerpt = implode(" ",$excerpt).'...';
	  } else {
	    $excerpt = implode(" ",$excerpt);
	  }	
	  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	  return $excerpt;
	}

	/**
	 * Register elements to be edited by the Headway Design Editor
	 **/

	function setup_elements() {


		$this->register_block_element(array(
			'id' => 'featured-title',
			'name' => 'FeaturedPosts Title',
			'selector' => '.featured-posts-featured .entry-title a',
			'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.featured-posts-featured .entry-title a:hover', 
			)
		));


		$this->register_block_element(array(
			'id' => 'flipper-meta',
			'name' => 'Flipper Meta',
			'selector' => '.flipper-meta',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.flipper-meta a:hover', 
			)
		));
		$this->register_block_element(array(
			'id' => 'flipper-post-title',
			'name' => 'Flipper Title',
			'selector' => 'h4.flipper-post-title',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => 'h4 .flipper-post-title a:hover', 
			)
		));
	
		$this->register_block_element(array(
			'id' => 'flipper-item',
			'name' => 'Flipper item',
			'selector' => '.flipper-item,',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.flipper-item a:hover', 
			)
		));
		$this->register_block_element(array(
			'id' => 'flipper-prev-next',
			'name' => 'Flipper prev next',
			'selector' => '.flipper-next, .flipper-prev',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.flipper-nex a:hover', 
			)
		));


		$this->register_block_element(array(
			'id' => 'flipper-next',
			'name' => 'Flipper next',
			'selector' => '.flipper-next',
			'parent' => 'flipper-prev-next',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.flipper-next a:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'flipper-prev',
			'name' => 'Flipper Prev',
			'parent' => 'flipper-prev-next',
			'selector' => '.flipper-prev',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.flipper-prev a:hover', 
			)
		));
		


	}


	public static function excerpt_more($more) {

		return '...';

	}

	
}