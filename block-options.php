<?php
class HeadwayFeaturedPostsBlockOptions extends HeadwayBlockOptionsAPI {
	
	
	public $tabs = array(
		'featured-setup' => 'Features Setup',
		'query-filters' => 'Query Filters',
		 'navigation' => 'Navigation & title',
		// 'images' => 'Images',
		// 'effects' => 'Effects',
		// 'social' => 'Social'
	);

	public $inputs = array(
		'featured-setup' => array(
			'total' => array(
				'type' => 'slider',
				'name' => 'total',
				'label' => 'Total',
				'slider-min' => 3,
				'slider-max' => 24,
				'slider-interval' => 1,
				'tooltip' => '',
				'default' => 3,
				'tooltip' => 'How many posts to show per row.',
				'callback' => ''
			),

			'columns' => array(
				'type' => 'slider',
				'name' => 'columns', 
				'label' => 'Columns',
				'slider-min' => 1,
				'slider-max' => 7,
				'slider-interval' => 1,
				'default' => 3,
				'tooltip' => 'Set how many pins to display horizontally'
			),
			
			'debug' => array(
				'type' => 'checkbox',
				'name' => 'debug', 
				'label' => 'Show: DEBUG',
				'default' => false,
				'tooltip' => 'Show a .',
			),



			// 'columns-smartphone' => array(
			// 	'type' => 'slider',
			// 	'name' => 'columns-smartphone', 
			// 	'label' => 'Columns (iPhone/Smartphone)',
			// 	'slider-min' => 1,
			// 	'slider-max' => 7,
			// 	'slider-interval' => 1,
			// 	'default' => 2,
			// 	'tooltip' => 'Set how many pins to display horizontally for iPhones and smartphones.  <strong>Recommended setting: 1 or 2</strong>'
			// ),

			// 'gutter-width' => array(
			// 	'type' => 'slider',
			// 	'name' => 'gutter-width', 
			// 	'label' => 'Gutter Width',
			// 	'slider-min' => 0,
			// 	'slider-max' => 100,
			// 	'slider-interval' => 1,
			// 	'default' => 15,
			// 	'unit' => 'px',
			// 	'tooltip' => 'The amount in space between pins horizontally.'
			// ),

			// 'featured-bottom-margin' => array(
			// 	'type' => 'slider',
			// 	'name' => 'featured-bottom-margin', 
			// 	'label' => 'Features Bottom Margin',
			// 	'slider-min' => 0,
			// 	'slider-max' => 50,
			// 	'slider-interval' => 1,
			// 	'default' => 15,
			// 	'unit' => 'px',
			// 	'tooltip' => 'The amount of space on the bottom of each pin.'
			// )
		),

		'query-filters' => array(
			'features-per-page' => array(
				'type' => 'integer',
				'name' => 'features-per-page',
				'label' => 'Featuress Per Page',
				'tooltip' => '',
				'default' => 10,
				'tooltip' => 'Determines how many pins to load at one time before loading more via pagination or <em>infinite scrolling</em>.'
			),
			
			'paginate' => array(
				'type' => 'checkbox',
				'name' => 'paginate',
				'label' => 'Paginate Featuress',
				'tooltip' => '',
				'default' => true,
				'tooltip' => 'Enabling pagination adds buttons to the bottom of the pin board to go to the next/previous page.  <strong>Note:</strong> If infinite scrolling is enabled, pagination will be hidden.'
			),

			'categories' => array(
				'type' => 'multi-select',
				'name' => 'categories',
				'label' => 'Categories',
				'tooltip' => '',
				'options' => 'get_categories()',
				'tooltip' => 'Filter the pins that are shown by categories.'
			),
			
			'categories-mode' => array(
				'type' => 'select',
				'name' => 'categories-mode',
				'label' => 'Categories Mode',
				'tooltip' => '',
				'options' => array(
					'include' => 'Include',
					'exclude' => 'Exclude'
				),
				'tooltip' => 'If this is set to <em>include</em>, then only the pins that match the categories filter will be shown.  If set to <em>exclude</em>, all pins that match the selected categories will not be shown.'
			),
			
			'post-type' => array(
				'type' => 'select',
				'name' => 'post-type',
				'label' => 'Post Type',
				'tooltip' => '',
				'options' => 'get_post_types()'
			),
			
			'author' => array(
				'type' => 'multi-select',
				'name' => 'author',
				'label' => 'Author',
				'tooltip' => '',
				'options' => 'get_authors()'
			),
			
			'order-by' => array(
				'type' => 'select',
				'name' => 'order-by',
				'label' => 'Order By',
				'tooltip' => '',
				'options' => array(
					'date' => 'Date',
					'title' => 'Title',
					'rand' => 'Random',
					'ID' => 'ID'
				)
			),
			
			'order' => array(
				'type' => 'select',
				'name' => 'order',
				'label' => 'Order',
				'tooltip' => '',
				'options' => array(
					'desc' => 'Descending',
					'asc' => 'Ascending',
				)
			)
		),
		'navigation' =>array(
			'prev-nav'	=> array(
				'name'	 => 'prev-nav',
				'type'	 => 'text',
				'label'	 => '< Prev',
				'tooltip' => 'tooltip'
			),
			'next-nav'	=> array(
				'name'	 => 'next-nav',
				'type'	 => 'text',
				'label'	 => 'Next >',
				'tooltip' => 'tooltip'
			),


			'title-html-tag'  => array(
				'type' => 'select',
				'name' => 'title-html-tag',
				'label' => 'Title HTML tag',
				'default' => 'h1',
				'options' => array(
					'h1' => '&lt;H1&gt;',
					'h2' => '&lt;H2&gt;',
					'h3' => '&lt;H3&gt;',
					'h4' => '&lt;H4&gt;',
					'h5' => '&lt;H5&gt;',
					'h6' => '&lt;H6&gt;',
					'span' => '&lt;span&gt;'
				)
			),

			'title-link'  => array(
				'type'  => 'checkbox',
				'name'  => 'title-link',
				'label' => 'Link Title?'
			),

			'title'	=> array(
				'name'	 => 'title',
				'type'	 => 'text',
				'label'	 => 'label',
				'tooltip' => 'tooltip'
			),

			
		),

		'text' => array(
			'show-titles' => array(
				'type' => 'checkbox',
				'name' => 'show-titles', 
				'label' => 'Show Titles',
				'default' => true
			),

			'content-to-show' => array(
				'type' => 'select',
				'name' => 'content-to-show', 
				'label' => 'Content To Show',
				'options' => array(
					'' => '&ndash; Do Not Show Content &ndash;',
					'excerpt' => 'Excerpts',
					'content' => 'Full Content'
				),
				'default' => 'excerpt',
				'tooltip' => 'The content is the written text or HTML for the entry.  This is edited in the WordPress admin panel.'
			),

			'show-author' => array(
				'type' => 'checkbox',
				'name' => 'show-author', 
				'label' => 'Meta: Show Author "byline"',
				'default' => false,
				'tooltip' => '<strong>Example:</strong> <em>by</em> Author Name'
			),

			'show-datetime' => array(
				'type' => 'checkbox',
				'name' => 'show-datetime', 
				'label' => 'Meta: Show Date/Time',
				'default' => false
			),

			'datetime-verb' => array(
				'type' => 'text',
				'name' => 'datetime-verb', 
				'label' => 'Meta: Posted Verb',
				'default' => 'Posted',
				'tooltip' => 'The posted verb will be placed before the time.  For instance, you may want to use "Listed" for real estate rather than "Posted"'
			),

			'relative-times' => array(
				'type' => 'checkbox',
				'name' => 'relative-times', 
				'label' => 'Meta: Use Relative Times',
				'default' => true,
				'tooltip' => '<strong>Example:</strong> 8 hours ago'
			)
		),

		'images' => array(
			'show-images' => array(
				'type' => 'checkbox',
				'name' => 'show-images', 
				'label' => 'Show Images',
				'default' => true,
			),

			'crop-vertically' => array(
				'type' => 'checkbox',
				'name' => 'crop-vertically', 
				'label' => 'Crop Vertically',
				'default' => false,
				'tooltip' => 'Trim all images to have the same height.  The trimmed/cropped height is roughly 75% of the width.'
			)
		),

		'effects' => array(
			'infinite-scroll' => array(
				'type' => 'checkbox',
				'name' => 'infinite-scroll', 
				'label' => 'Infinite Scrolling',
				'default' => true,
				'tooltip' => 'Infinite scrolling allows your visitors to view all of your pins without the need for them to click a button to continue to the next page.  The pins will be loaded automatically simply by scrolling.'
			),

			'hover-focus' => array(
				'type' => 'checkbox',
				'name' => 'hover-focus', 
				'label' => 'Hover Focus',
				'default' => false,
				'tooltip' => 'If enabled, the hovered pin will be focused while all others will be faded out.'
			),
		),

		'social' => array(
			'show-pinterest-button' => array(
				'type' => 'checkbox',
				'name' => 'show-pinterest-button', 
				'label' => 'Featuresterest: Show "Features It" Button',
				'default' => false,
				'tooltip' => 'Show a Featuresterest "Features It" button inside of the images.',
			),

			'show-twitter-button' => array(
				'type' => 'checkbox',
				'name' => 'show-twitter-button', 
				'label' => 'Twitter: Show Tweet Button',
				'default' => false,
				'tooltip' => 'Show a tweet button either inside of the post image or by the title.',
			),

			'twitter-username' => array(
				'type' => 'text',
				'name' => 'twitter-username', 
				'label' => 'Twitter: Your Username'
			),

			'twitter-hashtag' => array(
				'type' => 'text',
				'name' => 'twitter-hashtag', 
				'label' => 'Twitter: Hashtag to put in tweets (Optional)'
			),

			'show-facebook-button' => array(
				'type' => 'checkbox',
				'name' => 'show-facebook-button', 
				'label' => 'Facebook: Show Like/Share Button',
				'default' => false,
				'tooltip' => 'Show a Facebook share/like button either inside of the post image or by the title.',
			),

			'facebook-button-verb' => array(
				'type' => 'select',
				'label' => 'Facebook: Button Verb',
				'name' => 'facebook-button-verb',
				'options' => array(
					'like' => 'Like',
					'recommend' => 'Recommend'
				),
				'default' => 'like'
			)
		)
	);


	function get_categories() {
		
		$category_options = array();
		
		$categories_select_query = get_categories();
		
		foreach ($categories_select_query as $category)
			$category_options[$category->term_id] = $category->name;

		return $category_options;
		
	}
	
	
	function get_authors() {
		
		$author_options = array();
		
		$authors = get_users(array(
			'orderby' => 'post_count',
			'order' => 'desc',
			'who' => 'authors'
		));
		
		foreach ( $authors as $author )
			$author_options[$author->ID] = $author->display_name;
			
		return $author_options;
		
	}
	
	
	function get_post_types() {
		
		$post_type_options = array();

		$post_types = get_post_types(false, 'objects'); 
			
		foreach($post_types as $post_type_id => $post_type){
			
			//Make sure the post type is not an excluded post type.
			if(in_array($post_type_id, array('revision', 'nav_menu_item'))) 
				continue;
			
			$post_type_options[$post_type_id] = $post_type->labels->name;
		
		}
		
		return $post_type_options;
		
	}
	
	
}