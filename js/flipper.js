!function ($) {
	$(window).load(function(){
	
		$('.flipper').each(function(){
			
	    	var flipper = $(this)
			,	scrollSpeed
			, 	easing
			, 	shown = flipper.data('shown') || 3
			,	scrollSpeed = flipper.data('scroll-speed') || 1000
			,	easing = flipper.data('easing') || 'linear'
		//	,	easing = flipper.data('easing') || 'easeInOutCubic'
			
			
	    	flipper.carouFredSel({
	    		circular: 	true
	    		, responsive: true

				, height: "variable"
				, onCreate: function(){
					
				}

				
				, items       : {
					width : 240,
					height: "variable",
			        visible     : {
			            min         : 1
			            , max         : shown
			        }
			    }
				
			    , swipe       : {
			        onTouch     : true
			    }
			    , scroll: {
			    	easing          : easing
		            ,duration        : scrollSpeed
			        ,fx              : "crossfade"
					,pauseOnHover     : true
			    }
				
				
				,auto     : {
				        play            : true
				     ,timeoutDuration :  15000 //5 * auto.duration
		        ,duration        :  scroll.duration
				
				}
				
		        , prev    : {
			        button  : function() {
			           return flipper.parents('.flipper-wrap').prev(".flipper-heading").find('.flipper-prev');
			        }
		    	}
			    , next    : {
		       		button  : function() {
			           return flipper.parents('.flipper-wrap').prev(".flipper-heading").find('.flipper-next');
			        }
			    }
			    , auto    : {
			    	play: true
			    }
				
		    }).addClass('flipper-loaded').animate({'opacity': 1},1300);
		
		//	$.plCommon.plVerticalCenter('.flipper-info', '.pl-center', -20)
		
	    });
		
	})
}(window.jQuery);