<?php
/* Headway 3.2.5 has better support for the 'headway_element_data_defaults' filter */
if ( version_compare(HEADWAY_VERSION, '3.2.5', '>=') ) {

	add_filter('headway_element_data_defaults', 'featured_post_board_block_add_default_design_settings');
	function featured_post_board_block_add_default_design_settings($existing_defaults) {

		return array_merge($existing_defaults, featured_post_board_block_default_design_settings());

	}


} else {

	add_action('init', 'featured_post_board_pre325_default_design_settings');

	function featured_post_board_pre325_default_design_settings() {

		global $headway_default_element_data;

		$headway_default_element_data = array_merge($headway_default_element_data, featured_post_board_block_default_design_settings());

	}

}


function featured_post_board_block_default_design_settings() {

	return array(
			'block-pin-board-pin' => array(
				'properties' => array(
					'padding-top' => 1,
					'padding-right' => 1,
					'padding-bottom' => 1,
					'padding-left' => 1,

					'background-color' => 'ffffff',

					'border-color' => 'eeeeee',
					'border-style' => 'solid',
					'border-top-width' => 1,
					'border-right-width' => 1,
					'border-bottom-width' => 1,
					'border-left-width' => 1,

					'box-shadow-color' => 'eee',
					'box-shadow-blur' => 3,
					'box-shadow-horizontal-offset' => 0,
					'box-shadow-vertical-offset' => 2
				)
			),

			'block-pin-board-pin-title' => array(
				'properties' => array(
					'padding-top' => 15,
					'padding-right' => 15,
					'padding-left' => 15,

					'font-size' => 18,
					'line-height' => 120,

					'text-decoration' => 'none'
				),
				'special-element-state' => array(
					'hover' => array(
						'text-decoration' => 'underline'
					)
				)
			),

			'block-pin-board-pin-text' => array(
				'properties' => array(
					'font-size' => 12,
					'line-height' => 150,

					'padding-right' => 15,
					'padding-left' => 15
				)
			),

			'block-pin-board-pin-meta' => array(
				'properties' => array(
					'font-size' => 12,
					'line-height' => 120,

					'padding-right' => 15,
					'padding-left' => 15,

					'color' => '888888'
				)
			),

			'block-pin-board-pagination-button' => array(
				'properties' => array(
					'text-decoration' => 'none',
					'background-color' => 'eeeeee',

					'border-top-left-radius' => 4,
					'border-top-right-radius' => 4,
					'border-bottom-right-radius' => 4,
					'border-bottom-left-radius' => 4,

					'padding-top' => 5,
					'padding-right' => 9,
					'padding-bottom' => 5,
					'padding-left' => 9
				),
				'special-element-state' => array(
					'hover' => array(
						'background-color' => 'e7e7e7'
					)
				)
			)
		);

}